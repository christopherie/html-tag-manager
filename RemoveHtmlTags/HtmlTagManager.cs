﻿using System.Text;

namespace RemoveHtmlTags
{
    public class HtmlTagManager
    {
        public virtual string CustomiseFormattedPlainText(string formattedPlainText)
        {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append("<div class='bio'>");
            stringBuilder.AppendLine();
            stringBuilder.Append(formattedPlainText);
            stringBuilder.Append("</div>");
            return stringBuilder.ToString();
        }

        public string FormatPlainText(string cleanedHtmlString)
        {
            StringBuilder stringBuilder = new StringBuilder();
            foreach (char item in cleanedHtmlString)
            {
                if (item.Equals('.'))
                {
                    stringBuilder.Append(item);
                    stringBuilder.AppendLine();
                }
                else
                {
                    stringBuilder.Append(item);
                }
            }
            return stringBuilder.ToString();
        }

        public string GetPlaintextFromHtml(string source)
        {
            char[] array = new char[source.Length];
            int arrayIndex = 0;
            bool inside = false;

            for (int i = 0; i < source.Length; i++)
            {
                char let = source[i];
                if (let == '<')
                {
                    inside = true;
                    continue;
                }
                if (let == '>')
                {
                    inside = false;
                    continue;
                }
                if (!inside)
                {
                    array[arrayIndex] = let;
                    arrayIndex++;
                }
            }
            return new string(array, 0, arrayIndex);
        }
    }
}
