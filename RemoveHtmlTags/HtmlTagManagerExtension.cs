﻿using System.Text;

namespace RemoveHtmlTags
{
    public class HtmlTagManagerExtension : HtmlTagManager
    {
        public override string CustomiseFormattedPlainText(string formattedPlainText)
        {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append("<div class='resume'>");
            stringBuilder.AppendLine();
            stringBuilder.Append(formattedPlainText);
            stringBuilder.Append("</div>");
            return stringBuilder.ToString();
        }
    }
}
