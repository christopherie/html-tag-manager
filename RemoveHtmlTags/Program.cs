﻿using System;

namespace RemoveHtmlTags
{
    static class Program
    {
        static void Main(string[] args)
        {
            HtmlTagManager htmlTagManager = new HtmlTagManager();
            string htmlString = "<p>This has been a <span style='color: green;font-size:24px;'><i>wonderful</i></span> year.</p><p>This has been a <span style='color: green;font-size:24px;'><i>wonderful,</i></span> year.</p>";
            Console.WriteLine(htmlString);
            Console.WriteLine();
            string cleanedHtmlString = htmlTagManager.GetPlaintextFromHtml(htmlString);
            Console.WriteLine(cleanedHtmlString);
            Console.WriteLine();
            string formattedPlainText = htmlTagManager.FormatPlainText(cleanedHtmlString);
            Console.WriteLine(formattedPlainText);
            Console.WriteLine();
            string bioString = htmlTagManager.CustomiseFormattedPlainText(formattedPlainText);
            Console.WriteLine(bioString);
            Console.WriteLine();
            HtmlTagManagerExtension htmlTagManagerExtension = new HtmlTagManagerExtension();
            string resumeString = htmlTagManagerExtension.CustomiseFormattedPlainText(formattedPlainText);
            Console.WriteLine(resumeString);
            Console.WriteLine();
            Console.ReadLine();
        }
    }
}
